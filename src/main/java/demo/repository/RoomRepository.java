package demo.repository;

import demo.model.Room;

import java.util.List;
import java.util.Optional;
import java.util.stream.LongStream;

public class RoomRepository extends Repository<Room> {

    public RoomRepository(final int numberOfRooms){
        generateRooms(numberOfRooms);
    }

    @Override
    public Optional<List<Room>> findBy(final String attribute, final Object value) {
        if(!attribute.equalsIgnoreCase("id") || !(value instanceof Long))
            return Optional.empty();

        return findById((long) value).map(List::of);
    }

    private void generateRooms(final int numberOfRooms){
        if(numberOfRooms < 1)
            throw new IllegalArgumentException("Number of rooms must be greater than 1");

        LongStream.range(1, numberOfRooms+1).forEach(id -> cache.put(id, new Room(id)));
    }

}
