package demo.repository;

import demo.model.Guest;

import java.util.List;
import java.util.Optional;

public class GuestRepository extends Repository<Guest> {

    @Override
    public Optional<List<Guest>> findBy(final String attribute, final Object value) {
        if(!attribute.equalsIgnoreCase("name") || !(value instanceof String))
            return Optional.empty();

        final var guests = findBy(guest -> guest.getName().equalsIgnoreCase((String) value));

        if(guests.isEmpty())
            return Optional.empty();

        return Optional.of(guests);
    }

    public Optional<Guest> findByName(final String name) {
        return cache.values().stream().filter(guest -> guest.getName().equalsIgnoreCase(name)).findFirst();
    }

}
