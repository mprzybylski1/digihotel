package demo.repository;

import demo.model.BaseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class Repository<R> {

    protected final AtomicLong idSeq = new AtomicLong(1);
    protected final Map<Long, R> cache = new ConcurrentHashMap<>();

    public R save(final R item) {
        if(cache.containsKey(((BaseEntity)item).getId()))
            return cache.put(((BaseEntity) item).getId(), item);

        ((BaseEntity) item).setId(idSeq.getAndIncrement());
        cache.put(((BaseEntity) item).getId(), item);

        return item;
    }

    public Optional<R> findById(final Long id) {
        return Optional.ofNullable(cache.get(id));
    }

    public Optional<List<R>> findAll() {
        final var list = new ArrayList<>(cache.values());

        if(list.isEmpty())
            return Optional.empty();

        return Optional.of(list);
    }

    protected List<R> findBy(final Predicate<R> predicate){
        return cache.values().stream().filter(predicate).collect(Collectors.toList());
    }

    public abstract Optional<List<R>> findBy(final String field, final Object value);

}
