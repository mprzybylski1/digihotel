package demo.repository;

import demo.model.Booking;
import demo.model.Guest;
import demo.model.Room;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class BookingRepository extends Repository<Booking> {

    @Override
    public Optional<List<Booking>> findBy(final String field, final Object value) {

        if(field.equalsIgnoreCase("guest")){
            return fetchData(Guest.class, value, booking -> booking.getGuest().equals(value));
        }else if(field.equalsIgnoreCase("room")){
            return fetchData(Room.class, value, booking -> booking.getRoom().equals(value));
        }else if(field.equalsIgnoreCase("date")){
            return fetchData(LocalDate.class, value, booking -> booking.getDate().equals(value));
        }else
            return Optional.empty();
    }

    private Optional<List<Booking>> fetchData(final Class<?> clazz, final Object value, final Predicate<Booking> predicate){
        if (!clazz.isInstance(value))
            return Optional.empty();

        final var data = findBy(predicate);

        return data.isEmpty() ? Optional.empty() : Optional.of(data);
    }

}
