package demo.model;

import java.util.Objects;

public class Room implements BaseEntity {

    private long id;

    public Room(final long id) {
        this.id = id;
    }

    @Override
    public long getId(){
        return id;
    }

    @Override
    public void setId(final long id) {
        this.id = id;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Room room = (Room) o;
        return id == room.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                '}';
    }
}
