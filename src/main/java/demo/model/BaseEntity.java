package demo.model;

public interface BaseEntity {

    long getId();

    void setId(final long id);
}
