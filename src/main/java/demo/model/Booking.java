package demo.model;

import java.time.LocalDate;
import java.util.Objects;

public class Booking implements BaseEntity {

    private long id;
    private final Guest guest;
    private final Room room;
    private final LocalDate date;

    public Booking(final Guest guest, final Room room, final LocalDate date) {
        this.guest = guest;
        this.room = room;
        this.date = date;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(final long id) {
        this.id = id;
    }

    public Guest getGuest() {
        return guest;
    }

    public Room getRoom() {
        return room;
    }

    public LocalDate getDate() {
        return date;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Booking booking = (Booking) o;
        return id == booking.id && Objects.equals(guest, booking.guest) && Objects.equals(room, booking.room) && Objects.equals(date, booking.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, guest, room, date);
    }

    @Override
    public String toString() {
        return "Booking{" +
                "id=" + id +
                ", guest=" + guest +
                ", room=" + room +
                ", date=" + date +
                '}';
    }
}
