package demo.app;

import demo.exception.BookingException;
import demo.exception.GuestNotFoundException;
import demo.exception.RoomNotFoundException;
import demo.manager.BookingManager;
import demo.model.Booking;
import demo.model.Room;
import demo.repository.BookingRepository;
import demo.repository.GuestRepository;
import demo.repository.RoomRepository;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("InfiniteLoopStatement")
public class DigiHotel {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
    private static final Scanner scanner = new Scanner(System.in);
    private static BookingManager bookingManager;

    public static void main(String[] args) throws IOException {
        initalise();

        while(true){
            menu();
        }
    }

    private static void initalise(){
        printHome();

        System.out.print("Please enter number of rooms: ");

        final var roomRepository = new RoomRepository(scanner.nextInt());
        final var roomCache = roomRepository.findAll().orElseThrow(() -> new RoomNotFoundException("Could not find rooms"));

        bookingManager = new BookingManager(new BookingRepository(), new GuestRepository(), roomCache);
    }

    private static void printHome(){
        final var home = "\n" +
                " __          __  _                            _          _____  _       _ _    _       _       _ \n" +
                " \\ \\        / / | |                          | |        |  __ \\(_)     (_) |  | |     | |     | |\n" +
                "  \\ \\  /\\  / /__| | ___ ___  _ __ ___   ___  | |_ ___   | |  | |_  __ _ _| |__| | ___ | |_ ___| |\n" +
                "   \\ \\/  \\/ / _ \\ |/ __/ _ \\| '_ ` _ \\ / _ \\ | __/ _ \\  | |  | | |/ _` | |  __  |/ _ \\| __/ _ \\ |\n" +
                "    \\  /\\  /  __/ | (_| (_) | | | | | |  __/ | || (_) | | |__| | | (_| | | |  | | (_) | ||  __/ |\n" +
                "     \\/  \\/ \\___|_|\\___\\___/|_| |_| |_|\\___|  \\__\\___/  |_____/|_|\\__, |_|_|  |_|\\___/ \\__\\___|_|\n" +
                "                                                                   __/ |                         \n" +
                "                                                                  |___/                          \n";
        System.out.println(home);
    }

    private static int selectOption(){
        final var options = "Please select one of the following options:\n" +
                "1. Set number of rooms (currently " + bookingManager.getNumberOfRooms() + ")\n" +
                "2. Add booking\n" +
                "3. Find available rooms for given date\n" +
                "4. Find all bookings for guest\n" +
                "5. Exit";
        System.out.println(options);
        System.out.print(":");

        return scanner.nextInt();
    }

    private static void menu() throws IOException {
        final int option = selectOption();

        if(option == 1){
            optionOne();
        }else if(option == 2){
            optionTwo();
        }else if(option == 3){
            optionThree();
        }else if(option == 4){
            optionFour();
        }else if(option == 5){
            System.out.println("Exiting DigiHotel...");
            System.exit(0);
        }else{
            System.out.println("Selected option is not recognised");
            resetScreen();
        }
    }

    private static void optionOne(){
        System.out.println("Changing number of available rooms - Warning: This will reset cached data");
        clearScreen();
        initalise();
    }

    private static void optionTwo() throws IOException {
        resetScreen();

        System.out.println("Adding a new booking");

        System.out.print("Please enter guest name:");
        final var guestName = scanner.next();

        System.out.print("Please enter booking date:");
        final var date = getDate();

        try {
            final var booking = bookingManager.addBooking(guestName, date);
            System.out.println("New booking added: " + booking);
        } catch (RoomNotFoundException | IllegalArgumentException e) {
            System.err.println("Booking not added due: " + e.getMessage());
        }

        System.out.println("Please hit Enter to continue...");
        System.in.read();

        resetScreen();
    }

    private static void optionThree() throws IOException {
        resetScreen();

        System.out.println("Finding available rooms for selected date");

        System.out.print("Please enter date:");
        final var date = getDate();

        try {
            final var rooms = bookingManager.findAvailableRooms(date);
            printList(rooms.isEmpty(), "Could not find free room(s) for " + date,  "Found " + rooms.size() + " free room(s)", rooms.stream().map(Room::toString));
        } catch (Exception e) {
            System.err.println("Could not find rooms due: " + e.getMessage());
        }

        System.out.println("Please hit Enter to continue...");
        System.in.read();

        resetScreen();
    }

    private static void optionFour() throws IOException {
        resetScreen();

        System.out.println("Finding bookings for guest");

        System.out.print("Please enter guest name:");
        final var guestName = scanner.next();

        try {
            final var bookings = bookingManager.findAllBookings(guestName);

            printList(bookings.isEmpty(), "Could not find bookings for " + guestName, "Found " + bookings.size() + " booking(s)", bookings.stream().map(Booking::toString));
        } catch (GuestNotFoundException | BookingException | IllegalArgumentException e) {
            System.err.println("Could not find bookings due: " + e.getMessage());
        }

        System.out.println("Please hit Enter to continue...");
        System.in.read();

        resetScreen();
    }

    private static LocalDate getDate() {
        LocalDate date = null;

        while(date == null){
            var input = scanner.next();

            if(input.equalsIgnoreCase("q"))
                break;

            try {
                date = LocalDate.parse(input, formatter);
            } catch (DateTimeParseException e) {
                System.err.print(e.getMessage() + ". Please enter a new date that matches the following pattern dd/MM/yyyy or enter 'q': ");
            }
        }
        return date;
    }

    private static void printList(final boolean isEmpty, final String emptyListMsg, final String msg, final Stream<String> stringStream) {
        if(isEmpty)
            System.out.println(emptyListMsg);
        else {
            System.out.println(msg);
            System.out.println(stringStream.collect(Collectors.joining(", ")));
        }
    }

    private static void resetScreen() {
        clearScreen();
        printHome();
    }

    private static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

}
