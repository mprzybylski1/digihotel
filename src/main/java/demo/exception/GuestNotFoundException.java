package demo.exception;

public class GuestNotFoundException extends RuntimeException {

    public GuestNotFoundException(){
        super();
    }

    public GuestNotFoundException(final String msg){
        super(msg);
    }

}
