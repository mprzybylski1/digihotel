package demo.exception;

public class RoomNotFoundException extends RuntimeException {

    public RoomNotFoundException() {
        super();
    }

    public RoomNotFoundException(final String msg) {
        super(msg);
    }

}
