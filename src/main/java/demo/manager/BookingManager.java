package demo.manager;

import demo.exception.BookingException;
import demo.exception.GuestNotFoundException;
import demo.exception.RoomNotFoundException;
import demo.model.Booking;
import demo.model.Guest;
import demo.model.Room;
import demo.repository.GuestRepository;
import demo.repository.Repository;

import java.time.Instant;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class BookingManager {

    private final Repository<Booking> bookingRepository;
    private final Repository<Guest> guestRepository;

    private final List<Room> roomCache;

    public BookingManager(final Repository<Booking> bookingRepository, final Repository<Guest> guestRepository, final List<Room> roomCache){
        this.bookingRepository = bookingRepository;
        this.guestRepository = guestRepository;
        this.roomCache = roomCache;
    }

    public synchronized Booking addBooking(final String guestName, final LocalDate date){
        if(guestName == null)
            throw new IllegalArgumentException("Guest name must be not null");

        if(date == null)
            throw new IllegalArgumentException("Date must be not null");

        final var guest = ((GuestRepository) guestRepository).findByName(guestName).orElseGet(() -> guestRepository.save(new Guest(guestName)));

        final var room = findFirstAvailableRoom(date);

        final var savedBooking = bookingRepository.save(new Booking(guest, room, date));

        System.out.println("Saved booking: " + savedBooking + " at: " + Instant.now());

        return savedBooking;
    }

    public List<Room> findAvailableRooms(final LocalDate date) {
        if(date == null)
            throw new IllegalArgumentException("Date must be not null");

        final var availableRooms = new ArrayList<>(roomCache);

        bookingRepository
                .findBy("date", date)
                .map(bookingList -> bookingList.stream().map(Booking::getRoom).collect(Collectors.toList()))
                .ifPresent(availableRooms::removeAll);

        return availableRooms;
    }

    public List<Booking> findAllBookings(final String guestName){
        if(guestName == null)
            throw new IllegalArgumentException("Guest name must be not null");

        final var guest = ((GuestRepository) guestRepository).findByName(guestName)
                .orElseThrow(() -> new GuestNotFoundException("Could not find guest with following name: " + guestName));

        return bookingRepository.findBy("guest", guest)
                .orElseThrow(() -> new BookingException("Could not find bookings for guest " + guest));
    }

    private Room findFirstAvailableRoom(final LocalDate date){
        final var availableRooms = findAvailableRooms(date);

        if(availableRooms.isEmpty())
            throw new RoomNotFoundException("Could not find available rooms for date: " + date);

        return availableRooms.get(0);
    }

    public int getNumberOfRooms(){
        return roomCache.size();
    }

}
