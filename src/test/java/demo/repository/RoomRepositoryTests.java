package demo.repository;

import demo.model.Room;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("OptionalGetWithoutIsPresent")
public class RoomRepositoryTests {

    private RoomRepository roomRepository;

    private final int numberOfRooms = 10;
    private final long roomId = 1L;
    private Room savedRoom;

    @BeforeEach
    public void init(){
        roomRepository = new RoomRepository(numberOfRooms);
        savedRoom = roomRepository.save(new Room(roomId));
    }

    @Test
    void givenNumberOfRoomsOfNegative_thenIllegalArgumentExceptionIsThrown() {
        assertThrows(IllegalArgumentException.class, () -> new RoomRepository(-1));
    }

    @Test
    void givenNumberOfRoomsOfZero_thenIllegalArgumentExceptionIsThrown() {
        assertThrows(IllegalArgumentException.class, () -> new RoomRepository(0));
    }

    @Test
    void findById() {
        final var foundRoom = roomRepository.findById(roomId).get();
        assertEquals(savedRoom, foundRoom);
    }

    @Test
    void findByRoomId() {
        final var foundRooms = roomRepository.findBy("id", roomId).get();
        assertEquals(1, foundRooms.size());
        assertEquals(savedRoom, foundRooms.get(0));

        final var roomId2 = 20L;
        var optionalRooms = roomRepository.findBy("id", roomId2);
        assertFalse(optionalRooms.isPresent());

        optionalRooms = roomRepository.findBy("id", "roomId2");
        assertFalse(optionalRooms.isPresent());
    }

    @Test
    void findByUndefinedField() {
        final var foundRooms = roomRepository.findBy("test", roomId);
        assertFalse(foundRooms.isPresent());
    }

    @Test
    void findAllRooms() {
        final var allRooms = roomRepository.findAll().get();

        assertEquals(numberOfRooms, allRooms.size());
        assertEquals(1L, allRooms.get(0).getId());
        assertEquals(10L, allRooms.get(9).getId());
    }
}
