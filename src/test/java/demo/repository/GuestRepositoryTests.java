package demo.repository;

import demo.model.Guest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("OptionalGetWithoutIsPresent")
public class GuestRepositoryTests {

    private GuestRepository guestRepository;

    private final String guestName = "Marcin";
    private final long guestId = 1L;
    private Guest savedGuest;

    @BeforeEach
    public void init(){
        guestRepository = new GuestRepository();
        savedGuest = guestRepository.save(new Guest(guestName));
    }

    @Test
    void findById() {
        final var foundGuest = guestRepository.findById(guestId).get();
        assertEquals(savedGuest, foundGuest);
    }

    @Test
    void findBy() {
        final var foundGuests = guestRepository.findBy("name", guestName).get();
        assertEquals(1, foundGuests.size());
        assertEquals(savedGuest, foundGuests.get(0));

        final var guestName2 = "Marcin2";
        var optionalGuests = guestRepository.findBy("name", guestName2);
        assertFalse(optionalGuests.isPresent());

        optionalGuests = guestRepository.findBy("name", 2);
        assertFalse(optionalGuests.isPresent());
    }

    @Test
    void findByUndefinedField() {
        final var optionalGuests = guestRepository.findBy("test", guestName);
        assertFalse(optionalGuests.isPresent());
    }

    @Test
    void findByName() {
        final var foundGuest = guestRepository.findByName(guestName).get();
        assertEquals(savedGuest, foundGuest);

        final var guestName2 = "Marcin2";
        final var optionalGuest = guestRepository.findByName(guestName2);
        assertFalse(optionalGuest.isPresent());
    }

    @Test
    void findAllGuests() {
        final var guestName2 = "Marcin2";
        final var guestId2 = 2L;

        guestRepository.save(new Guest(guestName2));
        final var allGuests = guestRepository.findAll().get();

        assertEquals(2, allGuests.size());
        assertEquals(guestId, allGuests.get(0).getId());
        assertEquals(guestName, allGuests.get(0).getName());
        assertEquals(guestId2, allGuests.get(1).getId());
        assertEquals(guestName2, allGuests.get(1).getName());
    }
}
