package demo.repository;

import demo.model.Booking;
import demo.model.Guest;
import demo.model.Room;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("OptionalGetWithoutIsPresent")
public class BookingRepositoryTests {

    private BookingRepository bookingRepository;

    private final String guestName = "Marcin";
    private final long roomId = 1L;
    private final LocalDate today = LocalDate.now();
    private Booking savedBooking;
    private Guest guest;
    private Room room;

    @BeforeEach
    public void init(){
        bookingRepository = new BookingRepository();
        guest = new Guest(guestName);
        room = new Room(roomId);
        savedBooking = bookingRepository.save(new Booking(guest, room, today));
    }

    @Test
    void findByGuest() {
        final var foundBookings = bookingRepository.findBy("guest", guest).get();
        assertEquals(1, foundBookings.size());
        assertEquals(savedBooking, foundBookings.get(0));

        final var guestName2 = "Marcin2";
        final var guest2 = new Guest(guestName2);
        var optionalBookings = bookingRepository.findBy("guest", guest2);
        assertFalse(optionalBookings.isPresent());

        optionalBookings = bookingRepository.findBy("guest", "test");
        assertFalse(optionalBookings.isPresent());
    }

    @Test
    void findByRoom() {
        final var foundBookings = bookingRepository.findBy("room", room).get();
        assertEquals(1, foundBookings.size());
        assertEquals(savedBooking, foundBookings.get(0));

        final var roomId2 = 2L;
        final var room2 = new Room(roomId2);
        var optionalBookings = bookingRepository.findBy("room", room2);
        assertFalse(optionalBookings.isPresent());

        optionalBookings = bookingRepository.findBy("room", "test");
        assertFalse(optionalBookings.isPresent());
    }

    @Test
    void findByDate() {
        final var foundBookings = bookingRepository.findBy("date", today).get();
        assertEquals(1, foundBookings.size());
        assertEquals(savedBooking, foundBookings.get(0));

        var optionalBookings = bookingRepository.findBy("date", today.plusDays(1L));
        assertFalse(optionalBookings.isPresent());

        optionalBookings = bookingRepository.findBy("date", "test");
        assertFalse(optionalBookings.isPresent());
    }

    @Test
    void findByUndefinedField() {
        final var foundBookings = bookingRepository.findBy("test", today);
        assertFalse(foundBookings.isPresent());
    }

    @Test
    void givenNoBookings_whenCallingFindAllBookings_thenReturnEmptyOptional() {
        final var repository = new BookingRepository();
        final var allBookings = repository.findAll();

        assertFalse(allBookings.isPresent());
    }

    @Test
    void findById() {
        final var foundBooking = bookingRepository.findById(savedBooking.getId()).get();
        assertEquals(savedBooking, foundBooking);
    }

    @Test
    void findAllBookings() {
        final var guestName2 = "Marcin2";
        final long roomId2 = 2L;
        bookingRepository.save(new Booking(new Guest(guestName2), new Room(roomId2), today));

        final var allBookings = bookingRepository.findAll().get();
        assertEquals(2, allBookings.size());

        //First booking
        assertEquals(guestName, allBookings.get(0).getGuest().getName());
        assertEquals(roomId, allBookings.get(0).getRoom().getId());
        assertEquals(today, allBookings.get(0).getDate());

        //Second booking
        assertEquals(guestName2, allBookings.get(1).getGuest().getName());
        assertEquals(roomId2, allBookings.get(1).getRoom().getId());
        assertEquals(today, allBookings.get(1).getDate());
    }
}
