package demo.manager;

import demo.WorkerWithCountDownLatch;
import demo.exception.RoomNotFoundException;
import demo.model.Booking;
import demo.model.Room;
import demo.repository.BookingRepository;
import demo.repository.GuestRepository;
import demo.repository.RoomRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class BookingManagerTests {

    private static List<Room> roomCache;

    @BeforeAll
    public static void init(){
        final var roomRepository = new RoomRepository(20);
        roomCache = roomRepository.findAll().orElseThrow(() -> new RoomNotFoundException("Could not find rooms"));
    }

    @Test
    void givenNoBookingsAtSpecifiedDate_whenAddingNewBooking_thenCreateNewBooking_andAvailableRoomsShouldDecreaseByOne() {
        final var guestName = "Marcin";
        final var today = LocalDate.now();

        final var bookingManager = new BookingManager(new BookingRepository(), new GuestRepository(), roomCache);

        var availableRooms = bookingManager.findAvailableRooms(today);
        assertEquals(20, availableRooms.size());

        final var booking = bookingManager.addBooking(guestName, today);

        assertEquals(guestName, booking.getGuest().getName());
        assertEquals(today, booking.getDate());
        assertEquals(1, booking.getRoom().getId());

        availableRooms = bookingManager.findAvailableRooms(today);
        assertEquals(19, availableRooms.size());
    }

    @Test
    void givenAllRoomsBookedForGivenDate_whenAddingNewBooking_thenShouldThrowRoomNotFoundException() {
        final var today = LocalDate.now();
        final var bookingManager = new BookingManager(new BookingRepository(), new GuestRepository(), List.of(new Room(1)));

        var availableRooms = bookingManager.findAvailableRooms(today);
        assertEquals(1, availableRooms.size());

        bookingManager.addBooking("Marcin", today);

        availableRooms = bookingManager.findAvailableRooms(today);
        assertTrue(availableRooms.isEmpty());

        assertThrows(RoomNotFoundException.class, () -> bookingManager.addBooking("Marcin2", today));
    }

    @Test
    void givenEnoughRoomsAvailable_whenGuestBooksTwoRoomsForTheSameDate_thenRoomsAreAllocated() {
        final var guestName = "Marcin";
        final var today = LocalDate.now();

        final var bookingManager = new BookingManager(new BookingRepository(), new GuestRepository(), roomCache);

        var availableRooms = bookingManager.findAvailableRooms(today);
        assertEquals(20, availableRooms.size());

        bookingManager.addBooking(guestName, today);

        availableRooms = bookingManager.findAvailableRooms(today);
        assertEquals(19, availableRooms.size());

        bookingManager.addBooking(guestName, today);

        availableRooms = bookingManager.findAvailableRooms(today);
        assertEquals(18, availableRooms.size());

        final var allBookingsForGuest = bookingManager.findAllBookings(guestName);
        assertEquals(2, allBookingsForGuest.size());

        //First booking
        assertEquals(1, allBookingsForGuest.get(0).getId());
        assertEquals(guestName, allBookingsForGuest.get(0).getGuest().getName());
        assertEquals(1, allBookingsForGuest.get(0).getRoom().getId());
        assertEquals(today, allBookingsForGuest.get(0).getDate());

        //Second booking
        assertEquals(2, allBookingsForGuest.get(1).getId());
        assertEquals(guestName, allBookingsForGuest.get(1).getGuest().getName());
        assertEquals(2, allBookingsForGuest.get(1).getRoom().getId());
        assertEquals(today, allBookingsForGuest.get(1).getDate());
    }

    @Test
    void givenEnoughRoomsAvailable_whenGuestBooksTwoRoomsForDifferentDates_thenRoomsAreAllocated() {
        final var guestName = "Marcin";
        final var today = LocalDate.now();
        final var tomorrow = today.plusDays(1);

        final var bookingManager = new BookingManager(new BookingRepository(), new GuestRepository(), roomCache);

        var availableRooms = bookingManager.findAvailableRooms(today);
        assertEquals(20, availableRooms.size());

        bookingManager.addBooking(guestName, today);

        availableRooms = bookingManager.findAvailableRooms(today);
        assertEquals(19, availableRooms.size());

        availableRooms = bookingManager.findAvailableRooms(tomorrow);
        assertEquals(20, availableRooms.size());

        bookingManager.addBooking(guestName, tomorrow);

        availableRooms = bookingManager.findAvailableRooms(tomorrow);
        assertEquals(19, availableRooms.size());

        final var allBookingsForGuest = bookingManager.findAllBookings(guestName);
        assertEquals(2, allBookingsForGuest.size());

        //Booking for today
        assertEquals(1, allBookingsForGuest.get(0).getId());
        assertEquals(guestName, allBookingsForGuest.get(0).getGuest().getName());
        assertEquals(1, allBookingsForGuest.get(0).getRoom().getId());
        assertEquals(today, allBookingsForGuest.get(0).getDate());

        //Booking for tomorrow
        assertEquals(2, allBookingsForGuest.get(1).getId());
        assertEquals(guestName, allBookingsForGuest.get(1).getGuest().getName());
        assertEquals(1, allBookingsForGuest.get(1).getRoom().getId());
        assertEquals(tomorrow, allBookingsForGuest.get(1).getDate());
    }

    @Test
    void givenNoBookingsAtSpecifiedDate_thenReturnAllRooms() {
        final var today = LocalDate.now();
        final int expectedFirstRoomId = 1;
        final int expectedLastRoomId = 20;

        final var bookingManager = new BookingManager(new BookingRepository(), new GuestRepository(), roomCache);

        final var availableRooms = bookingManager.findAvailableRooms(today);

        assertEquals(20, availableRooms.size());
        assertEquals(expectedFirstRoomId, availableRooms.get(0).getId());
        assertEquals(expectedLastRoomId, availableRooms.get(19).getId());
    }

    @Test
    void givenTwoThreadsCreateBookingSimultaneously_when1RoomIsAvailable_thenCreateBookingForOneThread_andThrowRoomNotFoundForOther() throws InterruptedException {

        final var bookingManager = new BookingManager(new BookingRepository(), new GuestRepository(), List.of(new Room(1)));
        final Runnable createBooking = () -> bookingManager.addBooking("Marcin", LocalDate.now());

        final CountDownLatch latch = new CountDownLatch(1);
        final WorkerWithCountDownLatch worker1 = new WorkerWithCountDownLatch("Worker1", createBooking, latch);
        final WorkerWithCountDownLatch worker2 = new WorkerWithCountDownLatch("Worker2", createBooking, latch);

        worker1.start();
        worker2.start();

        latch.countDown();

        Thread.sleep(100);

        final List<Booking> bookings = bookingManager.findAllBookings("Marcin");
        assertEquals(1, bookings.size());
    }
}
