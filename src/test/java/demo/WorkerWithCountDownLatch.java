package demo;

import java.time.Instant;
import java.util.concurrent.CountDownLatch;

public class WorkerWithCountDownLatch extends Thread {
    private CountDownLatch latch;
    private Runnable runnable;

    public WorkerWithCountDownLatch(String name, Runnable runnable, CountDownLatch latch) {
        this.latch = latch;
        this.runnable = runnable;
        setName(name);
    }

    @Override
    public void run() {
        try {
            System.out.printf("[ %s ] created, blocked by the latch...\n", getName());
            latch.await();
            System.out.printf("[ %s ] starts at: %s\n", getName(), Instant.now());
            runnable.run();
        } catch (InterruptedException e) {
            // handle exception
        }
    }
}
