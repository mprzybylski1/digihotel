# DigiHotel

This is a pure core Java 11 application that allows to store bookings, retrieve available rooms for specified date, and retrieve all bookings made by a guest. The number of rooms available is configurable at the application startup.

The application has 5 different options:
* Option 1:- Allows to re-initialise the application with new number of rooms.
* Option 2:- Allows the use to make a booking.
* Option 3:- Allows the use to retrieve available rooms for selected date.
* Option 4:- Allows the user to retrieve all bookings made by the guest.
* Option 5:- Exists application.

## Usage
This repository contains already build jar called **digihotel-1.0-SNAPSHOT-jar-with-dependencies.jar** and in order to run it please clone this repository to you local space and go to the **target** directory. While in **target** directory please execute the following command to start the application:
````
java -jar digihotel-1.0-SNAPSHOT-jar-with-dependencies.jar
````